<?php

require_once('Frog.php');
require_once('Ape.php');



$sheep = new Animal;
$sheep->setname("shaun");

echo "Nama Hewan : " . $sheep->getname() . "<br>";
echo "Jumlah Kaki : ". $sheep->legs . "<br>";
echo "Cold blooded : ". $sheep->cold_blooded . "<br><br>";


$kodok = new Frog;
 $kodok->setname("Buduk");
 $kodok->setlegs("4");
 $kodok->setcold_blooded("no");

 echo "Nama Hewan : " . $kodok->getname() . "<br>";
 echo "Jumlah Kaki : " . $kodok->getlegs() . "<br>";
 echo "Cold blooded : " . $kodok->getcold_blooded() . "<br>";
 echo "suara/Bunyi : " . $kodok->jump . "<br><br>";

 $sunggokong = new Ape;
 $sunggokong->setname("Kera sakti");
 $sunggokong->setlegs("2");
 $sunggokong->setcold_blooded("no");
 
  echo "Nama Hewan : " . $sunggokong->getname() . "<br>";
  echo "Jumlah Kaki : " . $sunggokong->getlegs() . "<br>";
  echo "Cold blooded : " . $sunggokong->getcold_blooded() . "<br>";
  echo "suara/Bunyi : " . $sunggokong->yell . "<br><br>";
 
 
?>