<?php

class Animal{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function setname($nama){
        $this->name = $nama;
    }
    public function setlegs($nama){
        $this->legs = $nama;
    }
    public function setcold_blooded($nama){
        $this->cold_blooded = $nama;
    }

    public function getname(){
        return $this->name;
    }
    public function getlegs(){
        return $this->legs;
    }
    public function getcold_blooded(){
        return $this->cold_blooded;
    }
}



?>